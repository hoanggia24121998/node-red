FROM node:19-buster-slim

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --omit=dev

# Bundle app source
COPY settings.js ./
COPY flows_cred.json ./
COPY flows.json ./

EXPOSE 1880
CMD [ "node", "./node_modules/node-red/red.js", "./flows.json", "-settings", "./settings.js" ]