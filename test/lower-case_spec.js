var helper = require("node-red-node-test-helper");
var lowerNode = require("../lib/node-red-contrib-example-lower-case/lower-case.js");
var functionNode = require("../node_modules/node-red/nodes/core/core/80-function.js")

helper.init(require.resolve('node-red'));

function spyOn(nodeIds){
  nodeIds.forEach((id) => {
   const nodeName =  helper.getNode(id).name
   var n1 = helper.getNode(id)
      helper.getNode(id).on("input", (msg) => {
          console.log(`NODE: ${nodeName}`, msg);
          if(nodeName === "demo"){
            n1.should.have.property('a', 'Helllo')
          }
          n1.should.have.property('payload', 'aaaa');
      });
  });
}

describe('lower-case Node', function () {

  beforeEach(function (done) {
    helper.startServer(done);
  });

  afterEach(function (done) {
    helper.unload();
    helper.stopServer(done);
  });

  
  it('should be loaded', function (done) {
    var flow = [{ id: "4dd25221d3807a9a", type: "lower-case", name: "test name" }];
    helper.load(lowerNode, flow, function () {
      var n1 = helper.getNode("4dd25221d3807a9a");
      n1.should.have.property('name', 'test name');
      done();
    });
  });

  it('should be loaded v2', function (done) {
    var flow = [{ id: "b1664614725acb2d", type: "function", name: "example function" }];
    helper.load(functionNode, flow, function () {
      var n1 = helper.getNode("b1664614725acb2d");
      n1.should.have.property('name', 'example function');
      done();
    })
  });

  // it('should be loaded v3', function (done) {
  //   var flow = [{ id: "4d6b05bfab93a35f", type: "function", name: "demo"}];
  //   helper.load(functionNode, flow, function () {
  //     var n1 = helper.getNode("4d6b05bfab93a35f");
  //     n1.should.have.property('name', 'function 2');
  //     done();
  //   })
  // })

  // it('should be loaded v4', function (done) {
  //   var flow = [
  //   { id: "b1664614725acb2d", type: "function", name: "example function", wires:[["4d6b05bfab93a35f"]] }, 
  //   { id: "4d6b05bfab93a35f", type: "function", name: "demo"}
  //   ];

  //   helper.load(functionNode, flow, () => {
  //     spyOn(["b1664614725acb2d", "4d6b05bfab93a35f"]);
  //     var n1 = helper.getNode("b1664614725acb2d");
  //     // n1.receive({ payload: "aaaa" })
  //     n1.send({ payload: "aaaa" });
  // })
  // done();
  // }).timeout(10000);

  // it('should make payload lower case', function (done) {
  //   var flow = [{ id: "4dd25221d3807a9a", type: "lower-case", name: "test name",wires:[["n2"]] },
  //   { id: "n2", type: "helper" }];
  //   helper.load(lowerNode, flow, function () {
  //     var n2 = helper.getNode("n2");
  //     var n1 = helper.getNode("n1");
  //     n2.on("input", function (msg) {
  //       console.log(msg)
  //       msg.should.have.property('payload', 'uppercase');
  //       done();
  //     });
  //     n1.receive({ payload: "UpperCase" });
  //   });
  // });
});